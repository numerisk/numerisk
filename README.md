 ## Hey! I'm Matheus  ![Profile View Counter](https://komarev.com/ghpvc/?username=MatheusZilberman)                                     

 <p>
 Hello friends of GitHub, I develop software for 2 years I started with Python and at the<br>
 moment I develop as a full stack. I love the open source community and am a<br> 
 cryptocurrency enthusiast.<br>
</p> 
 
 <div>
  
  <img height="180em" src="https://github-readme-stats.vercel.app/api?username=MatheusZilberman&show_icons=true&theme=dark&include_all_commits=true&count_private=true"/>
  <img height="180em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=MatheusZilberman&layout=compact&langs_count=16&theme=dark"/>
 
 </div>

  
<div>
 
 <img align="center" alt="icon-HTML" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original.svg"> 
 <img align="center" alt="icon-CSS" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original.svg"> 
 <img align="center" alt="icon-Js" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-plain.svg">    <img align="center" alt="icon-Ts" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/typescript/typescript-plain.svg">    <img align="center" alt="icon-React" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/react/react-original.svg">
 <img align="center" alt="icon-Csharp" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/csharp/csharp-original.svg">

</div>
 


